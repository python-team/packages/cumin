cumin (5.0.0-1) unstable; urgency=medium

  * v5.0.0 upstream release (Closes: #1042262)
  * Bumped Standards-Version to 4.7.0.
  * Set minimum Python version to 3.9.
  * Updated Copyright year.

 -- Riccardo Coccioli <rcoccioli@wikimedia.org>  Thu, 16 Jan 2025 19:00:06 -0000

cumin (4.2.0-3) unstable; urgency=medium

  * Make manpage build step compatible with newer Sphinx 7.1 using
   sphinx-build instead of setup.py build_sphinx.

 -- Riccardo Coccioli <rcoccioli@wikimedia.org>  Fri, 04 Aug 2023 15:38:12 -0000

cumin (4.2.0-2) unstable; urgency=medium

  * Bump Standards-Version to 4.6.2.
  * Change Architecture from any to all as the package is pure Python.
  * Add autopkgtest configuration for automated testing.

 -- Riccardo Coccioli <rcoccioli@wikimedia.org>  Tue, 17 Jan 2023 19:45:38 -0000

cumin (4.2.0-1) unstable; urgency=medium

  * v4.2.0 upstream release. (Closes: #1026005)

 -- Riccardo Coccioli <rcoccioli@wikimedia.org>  Thu, 12 Jan 2023 18:06:38 -0000

cumin (4.1.1-2) unstable; urgency=medium

  * upload to experimental (Closes: #924685)

 -- Antoine Beaupré <anarcat@debian.org>  Wed, 23 Nov 2022 09:49:53 -0500

cumin (4.1.1-1) unstable; urgency=medium

  * v4.1.1 upstream release.

 -- Riccardo Coccioli <rcoccioli@wikimedia.org>  Wed, 23 Jun 2021 13:23:19 -0000

cumin (4.1.0-1) unstable; urgency=medium

  * v4.1.0 upstream release.
  * Removed python3-flake8 from Build-Depends.

 -- Riccardo Coccioli <rcoccioli@wikimedia.org>  Mon, 03 May 2021 11:06:40 -0000

cumin (4.0.0-1) buster-wikimedia; urgency=medium

  * v4.0.0 upstream release, no code changes.

 -- Riccardo Coccioli <rcoccioli@wikimedia.org>  Thu, 10 Sep 2020 15:46:46 +0200

cumin (4.0.0~rc1-1) buster-wikimedia; urgency=medium

  * v4.0.0rc1 upstream release.

 -- Riccardo Coccioli <rcoccioli@wikimedia.org>  Tue, 09 Jun 2020 18:41:29 +0200

cumin (3.0.2-1) jessie-wikimedia; urgency=medium

  * v3.0.2 upstream release.

 -- Riccardo Coccioli <rcoccioli@wikimedia.org>  Mon, 30 Jul 2018 12:26:33 +0200

cumin (3.0.1-1) jessie-wikimedia; urgency=medium

  * v3.0.1 upstream release.

 -- Riccardo Coccioli <rcoccioli@wikimedia.org>  Mon, 19 Feb 2018 22:35:42 +0100

cumin (3.0.0-1) jessie-wikimedia; urgency=medium

  * v3.0.0 upstream release.
  * Migration to Python 3, dropping support of Python 2.

 -- Riccardo Coccioli <rcoccioli@wikimedia.org>  Mon, 19 Feb 2018 16:54:25 +0100

cumin (2.0.0-1) jessie-wikimedia; urgency=medium

  * v2.0.0 upstream release.

 -- Riccardo Coccioli <rcoccioli@wikimedia.org>  Fri, 19 Jan 2018 17:48:55 +0100

cumin (1.3.0-1) jessie-wikimedia; urgency=medium

  * v1.3.0 upstream release.
  * Fix upstream-tag in debian/gbp.conf.

 -- Riccardo Coccioli <rcoccioli@wikimedia.org>  Fri, 03 Nov 2017 16:05:12 +0100

cumin (1.2.2-1) jessie-wikimedia; urgency=medium

  * v1.2.2 upstream release.
  * Bump Standards-Version to 3.9.8
  * Change Section to utils
  * Add Sphinx-related Build-Depends in order to build documentation and manpage
  * Override dh_installman to generate the manpage that will be installed
  * Add cumin.manpages to let dh install the manpage
  * Add OpenStack backend dependencies as Suggests, given that they are now
    optional in setup.py
  * Add upstream signing GPG key
  * Add watch file to track GitHub releases

 -- Riccardo Coccioli <rcoccioli@wikimedia.org>  Wed, 11 Oct 2017 22:43:05 +0200

cumin (1.2.1-1) jessie-wikimedia; urgency=medium

  * 1.2.1 upstream release.

 -- Riccardo Coccioli <rcoccioli@wikimedia.org>  Wed, 27 Sep 2017 16:04:24 +0200

cumin (1.1.1-1) jessie-wikimedia; urgency=medium

  * 1.1.1 upstream release.
  * Improve version detection for setuptools-scm.

 -- Riccardo Coccioli <rcoccioli@wikimedia.org>  Tue, 26 Sep 2017 11:04:24 +0200

cumin (1.1.0-1) jessie-wikimedia; urgency=medium

  * 1.1.0 upstream release.
  * Update Build-Depends.
  * Revert example files hotfix for https://phabricator.wikimedia.org/T174008.
  * Fix tests run with setuptools-scm

 -- Riccardo Coccioli <rcoccioli@wikimedia.org>  Thu, 21 Sep 2017 23:02:24 +0200

cumin (1.0.0-1) jessie-wikimedia; urgency=medium

  * 1.0.0 upstream release.
  * Add new Build-Depends required by upstream
  * Bump debhelper to >= 10
  * Migrate build system to pybuild
  * Use pytest to run the tests
  * Override dh_auto_test to only run unit tests, to avoid the additional
    dependencies for the other static analysis tools.

 -- Riccardo Coccioli <rcoccioli@wikimedia.org>  Wed, 23 Aug 2017 23:02:24 +0200

cumin (0.0.2-1) jessie-wikimedia; urgency=medium

  * Upgrade to version 0.0.2.
  * Add support for batch processing.
  * Minor fixes and improvements.

 -- Riccardo Coccioli <rcoccioli@wikimedia.org>  Wed, 15 Mar 2017 18:12:24 +0100

cumin (0.0.1-1) jessie-wikimedia; urgency=medium

  * Initial packaging.

 -- Riccardo Coccioli <rcoccioli@wikimedia.org>  Thu, 16 Feb 2017 11:15:24 +0100
